﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWithLinq.Forms
{
    public partial class frm_Stores : XtraForm
    {
        DAL.Store store;
        public frm_Stores()
        {
            InitializeComponent();
            New();
        }
        public frm_Stores(int id)
        {
            InitializeComponent();
            var db = new DAL.dbDataContext();

            store = db.Stores.Where(s => s.ID == id).First();
            GetData();
        }
        void Save()
        {
            if (textEdit1 .Text.Trim() == string.Empty)
            {
                textEdit1.ErrorText = "برجاء ادخال اسم الفرع";
                return;
            }


            var db = new DAL.dbDataContext();

            if(store .ID == 0 )
            db.Stores.InsertOnSubmit(store);
            else 
            db.Stores.Attach(store);


            SetData();
            db.SubmitChanges();
            XtraMessageBox.Show("تم الحفظ بنجاح");

        }
        void GetData()
        {
            textEdit1.Text = store.Name; 
        }
        void SetData()
        {
            store.Name = textEdit1.Text;
        }
        void New()
        {
            store = new DAL.Store();
            GetData();
        }

        private void btn_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var db = new DAL.dbDataContext();
            if(XtraMessageBox.Show(text: "هل تريد حذف المخزن",caption: "تاكيد الحذف",
                buttons: MessageBoxButtons.YesNo,
                icon: MessageBoxIcon.Question ) == DialogResult.Yes )
            {
                db.Stores.Attach(store);
                db.Stores.DeleteOnSubmit(store);
                db.SubmitChanges();
                XtraMessageBox.Show("تم الحذف بنجاح");
                New();
            }
            

        }
    }
}
