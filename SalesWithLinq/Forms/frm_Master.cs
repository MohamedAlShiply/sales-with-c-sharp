﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWithLinq.Forms
{
    public partial class frm_Master : XtraForm
    {
        
        public string ErrorText { get {
                 
                return "هذا الحقل مطلوب";
            } 
        }
        public frm_Master()
        {
            InitializeComponent(); 
        }
        public virtual void Save()
        {
            XtraMessageBox.Show("تم الحفظ بنجاح");
            RefreshData();

        }
        public virtual void New()
        {
            GetData();
        }
        public virtual void Delete()
        {

        }
        public virtual void GetData()
        {
             
        }
        public virtual void SetData()
        {

        }
        public virtual void RefreshData()
        {

        }
        private void btn_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Delete();
        }

        private void frm_Master_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void frm_Master_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Save();
            }
            if (e.KeyCode == Keys.F2)
            {
                New();
            }
            if (e.KeyCode == Keys.F3)
            {
                Delete();
            }
        }
    }
}
